from django.shortcuts import render, redirect
from .models import Schedule
from . import forms
# Create your views here.

def about(request):
    return render(request, 'about.html')

def skills(request):    
    return render(request, 'skills2.html')

def experiences(request):
    return render(request, 'experiences.html')

def gallery(request):
    return render(request, 'gallery.html')

def contact(request):
    return render(request, 'contact.html')

def schedule(request):
    schedules = Schedule.objects.all().order_by('date')
    return render(request, 'schedule.html', {'schedules': schedules})

def schedule_create(request):
    if request.method == 'POST':
        form = forms.ScheduleForm(request.POST)
        if form.is_valid():
            form.save()
            return redirect('mainpage:schedule')

    else:
        form = forms.ScheduleForm()
    return render(request, 'schedule_create.html', {'form': form})


def schedule_delete(request):
	Schedule.objects.all().delete()
	return render(request, "schedule.html")

# def schedule_deleteone(request, idTemp):
#     schedule = Schedule.objects.get(id=idTemp)
#     schedule.delete()
#     return redirect('schedule')

def schedule_deletesatu(request, id):
    sched = Schedule.objects.get(id=id)
    
    sched.delete()
    return redirect('mainpage:schedule')


    