from django.urls import path
from . import views

app_name = 'mainpage'


urlpatterns = [
    path('', views.about, name = 'home'),
    path('about/', views.about, name = 'about'),
    path('skills/', views.skills, name = 'skills'),
    path('experiences/', views.experiences, name = 'experiences'),
    path('gallery/', views.gallery, name = 'gallery'),
    path('contact/', views.contact, name = 'contact'),
    path('schedule/', views.schedule, name='schedule'),
    path('schedule/create', views.schedule_create, name='schedule_create'),
    path('schedule/delete', views.schedule_delete, name='schedule_delete'),
    # path('schedule/deleteone/<int:id>', views.schedule_deleteone, name='schedule_deleteone'),
    path('schedule/deletesatu/<int:id>', views.schedule_deletesatu, name='schedule_deletesatu'),
]